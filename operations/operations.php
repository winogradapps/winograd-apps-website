<?php

$name = $_FILES["fileToUpload"]["tmp_name"];

$data = file_get_contents($name);
$rows = explode("\n",$data);

$unit_data = explode("\t", $rows[0]);
$unit_ratio = $unit_data[0];

// print_r($unit_ratio);
print_r("Definitions:\n");
print_r("input unit = unit of time used for cycle time\n");
print_r("output unit = unit of time used for output numbers\n");
print_r("\n\n");
print_r("***** Analysis *****\n");


$found = false;

$steps = array();

foreach($rows as $r){
	$parts = explode("\t",$r);

	if ($found){
		$s = new Step($r, $unit_ratio);
		array_push($steps, $s);
	}

	if ($parts[0]=="Step"){
		$found = true;
	}

	foreach($steps as $step){
		// $step->calculateRate();
	}
	$counter = 0;
	do {
		$counter++;
		// print_r("\n".$counter."\n");

		foreach($steps as $s){
			$times = array();
			foreach($s->required_steps as $rs){
				array_push($times, getStepMinTime($steps, $rs, $unit_ratio));
			}

			$ready_for_step = true;
			foreach($times as $t){
				if (strlen($t) == 0){
					$ready_for_step = false;
				}
			}

			if ($ready_for_step){
				@$s->min_completed_time = max($times) + $s->cycle_time;
			}
			// print_r($times);

		}


	} while (!isMinTimeSolved($steps) && $counter < 1000);
	

}

$bottleneck_rate = getBottleneckRate($steps, $unit_ratio);

foreach($steps as $s){
	$s->describe();
}

print_r("TFE: ".getTFE($steps, true, $unit_ratio)." input units..."."TFE: ".round(getTFE($steps, false, $unit_ratio),5)." output units.\n");
print_r("Bottle neck rate: ".$bottleneck_rate." per output unit\n");
print_r("Bottle neck step(s): ".getBottleneckSteps($steps, $bottleneck_rate, $unit_ratio)."\n");
print_r("Labor Utilization: ".round(getLaborUtilization($steps, $bottleneck_rate, $unit_ratio),5)."\n");

function getTFE($steps, $original, $unit_ratio){
	$max = 0;
	foreach($steps as $s){
		if ($s->min_completed_time > $max){
			$max = $s->min_completed_time;
		}
	}
	if ($original){
		return $max;
	}
	return $max / $unit_ratio;
}

function getBottleneckRate($steps, $unit_ratio){
	$rate = 99999999;

	foreach($steps as $s){
		if ($s->rate < $rate){
			$rate = $s->rate;
		}
	}
	return $rate * $unit_ratio;
}

function getBottleneckSteps($steps, $rate, $unit_ratio){
	$output = array();

	foreach($steps as $s){
		if ($s->rate * $unit_ratio == $rate){
			array_push($output, $s->name);
		}
	}
	return implode(", ", $output);
}


function getStepMinTime($steps, $step_number, $unit_ratio){
	$step_number = str_replace("\"", "", $step_number);
	if ($step_number == 0){
		return 0;
	}
	foreach($steps as $s){
		if ($s->step_number == $step_number){
			return $s->min_completed_time;
		}
	}
}

function getLaborUtilization($steps, $bottleneck_rate, $unit_ratio){
	$numerator = 0;
	$denominator = 0;

	print_r("\n\nLabor Utilization Calculation...\n\n");

	foreach($steps as $s){

		$num = $s->workers * $bottleneck_rate/($s->rate*$unit_ratio/$s->quantity_per_unit);
		// print_r("STEP ".$s->step_number." - ".$num." with ". $s->workers." workers"."\n");

		print_r("STEP ".$s->step_number." - ".$s->workers." * ". $bottleneck_rate." / ".($s->rate*$unit_ratio/$s->quantity_per_unit)."\n");		
		$numerator += $num;
		$denominator += $s->workers;
	}
		print("Denominator: ".$denominator."\n\n");
	return $numerator/$denominator;

}
// print_r($steps);

print_r("\n\n");

function isMinTimeSolved($steps){
	$solved = true;
	foreach($steps as $s){
		if ($s->min_completed_time == null){
			$solved = false;
		}
	}
	return $solved;
}

class Step {
	public $unit_ratio;
	public $step_number;
	public $name;
	public $required_steps;
	public $workers;
	public $stations;
	public $quantity_per_unit;
	public $inventory_per_station;
	public $cycle_time;
	public $rate;
	public $min_completed_time = null;

	function __construct($row, $unit_ratio)
    {
    	// print("HERE");
     //    print_r($row);
     //    print_r("\n");
        $parts = explode("\t",$row);
        $this->unit_ratio = $unit_ratio;
        $this->step_number = $parts[0];
        $this->name = $parts[1];
        $this->required_steps = explode(",",$parts[2]);

        $this->workers = $parts[3];
        $this->stations = $parts[4];
        $this->quantity_per_unit = $parts[5];
        $this->inventory_per_station = $parts[6];
        $this->cycle_time = $parts[7];

        @ $this->rate = $this->stations * $this->inventory_per_station / $this->cycle_time;

    }

    function getI(){
    	return $this->inventory_per_station * $this->stations;
    }

    function getR(){
    	@ $r =  $this->getI()*$this->unit_ratio/$this->cycle_time;
    	return $r;
    }

    function describe(){
    	print_r("• ".$this->name."\n\t");
    	echo("Step Inventory: ".$this->getI()."\n\t");
    	echo("Cycle Time: ".$this->cycle_time." input units\n\t");
    	print_r("Production Rate: ".$this->getR()." output units \n");
    	
    	print_r("\n");
    }
}
?>