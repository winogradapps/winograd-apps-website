<?php

$items = array();
http://www.atlascarpetmills.com/


array_push($items, array("acme.jpg", "http://www.acmebricktileandstone.com/"));
array_push($items, array("atlas.svg", "http://www.atlascarpetmills.com/"));
array_push($items, array("bpi.svg", "https://bpiteam.com/"));
array_push($items, array("centiva.jpg", "https://www.tandus-centiva.com/"));
array_push($items, array("daltile.png", "http://www.daltile.com/"));
array_push($items, array("gotitcovered.png", "http://wegotitcoveredflooring.com//"));
array_push($items, array("jjflooring.svg", "http://www.jjflooringgroup.com/"));
array_push($items, array("earthwerks.png", "http://www.earthwerks.com/"));
array_push($items, array("mohawk.png", "https://www.mohawkflooring.com"));
array_push($items, array("shaw.png", "https://www.shawfloors.com"));
array_push($items, array("patcraft.jpg", "http://www.patcraft.com/"));
array_push($items, array("engineered.png", "https://www.engineeredfloors.com"));
array_push($items, array("metroflor.png", "http://www.metroflorusa.com/artistek.aspx"));
array_push($items, array("armstrong.png", "https://www.armstrong.com/flooring/products"));
array_push($items, array("leggett.png", "http://www.leggett.com"));
array_push($items, array("swifftrain.png", "http://www.swiff-train.com/"));

sort($items);	
echo "\n";

foreach($items as $item){
	$text = '<div data-aos="flip-up" class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 project-item mix workspace">'.
              '<div class="thumb-2">'.
                '<div class="image">'.
                  '<img src="images/vendors/'.$item[0].'"></div>'.
                  '<div class="hover-effect">'.
                    '<a href="'.$item[1].'" target="_blank">'.
                      '<i class="fa fa-search"></i>'.
                    '</a></div></div></div>';

    echo $text."\n";

}
?>